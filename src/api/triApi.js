export function fetchTrivia(){
    return fetch('https://opentdb.com/api.php?amount=10&category=11&difficulty=medium&type=multiple')
        .then(response => response.json())
}
