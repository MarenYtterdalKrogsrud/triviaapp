import VueRouter from 'vue-router'
import StartScreen from "@/components/StartScreen";
import Trivia from "@/components/Trivia";

/*Router redirects to different paths*/
const routes = [
    {
        path: '/',
        component: StartScreen
    },
    {
        path: '/trivia',
        component: Trivia
    }
];

const router = new VueRouter( { routes } )

export default router;
