## Vue Trivia Game

### Description
The Vue Trivia Game is written using Vue, and the open trivia db API.
The API linked was generated through this [website](https://opentdb.com/api_config.php).
The app works best in Chrome. 
 
We chose 10 questions of medium difficulty, in the movie category.
 
The Trivia game has a start screen, questions and a result screen containing 
the score.

* Start Screen 

This component has a button to start the game. This button redirects the user
to the questions component.

---

* Questions

Once the game starts, the app fetches the questions from the API set up.
The application displays only one question at a time. It is a multiple choice,
so the user can choose between four different alternatives. Once a question
is answered, the app moves on to the next question. On the bottom of the screen,
the user can se what questions they have answered, and how many they have left.
When all the questions have been answered, the user is presented with a screen
that displays all the questions. This is the result screen.

---

* Result screen

Each question counts 10 points. The result screen displays the score, and the
questions with the correct answers, and the answers the user chose.

--- 

The styling for the text on the start screen has been found at this 
[resource](https://codepen.io/erikjung/pen/XdWEKE).

---

Application made by:
Maren Ytterdal Krogsrud and
Sarah Thorstensen

---

Project can be found at [heroku](https://triviamovieapp.herokuapp.com/)

Project repo can be found at: [GitLab](https://gitlab.com/MarenYtterdalKrogsrud/triviaapp)
